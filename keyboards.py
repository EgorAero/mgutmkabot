from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


async def kb_start():
    bt1 = InlineKeyboardButton('Найти', callback_data='start')
    kb = InlineKeyboardMarkup()
    kb.row(bt1)
    return kb


async def kb_inst(a):
    kb = InlineKeyboardMarkup()
    for i in a:
        kb.row(InlineKeyboardButton(i[1], callback_data='university_%s' % i[0]))
    return kb


async def kb_name(a):
    kb = InlineKeyboardMarkup()
    for i in a:
        kb.row(InlineKeyboardButton(i[1], callback_data='name_%s' % i[0]))
    return kb


async def kb_degr():
    bt1 = InlineKeyboardButton('Бакалавр', callback_data='bac')
    bt2 = InlineKeyboardButton('Магистр', callback_data='mag')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2)
    bt0 = InlineKeyboardButton('Назад', callback_data='start')
    kb.row(bt0)
    return kb


async def kb_bac():
    bt1 = InlineKeyboardButton('1', callback_data='bac_1')
    bt2 = InlineKeyboardButton('2', callback_data='bac_2')
    bt3 = InlineKeyboardButton('3', callback_data='bac_3')
    bt4 = InlineKeyboardButton('4', callback_data='bac_4')
    bt5 = InlineKeyboardButton('Выпускник', callback_data='bac_5')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2, bt3, bt4)
    kb.row(bt5)
    bt0 = InlineKeyboardButton('Назад', callback_data='start')
    kb.row(bt0)
    return kb


async def kb_mag():
    bt1 = InlineKeyboardButton('1', callback_data='mag_1')
    bt2 = InlineKeyboardButton('2', callback_data='mag_2')
    bt3 = InlineKeyboardButton('Выпускник', callback_data='mag_5')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2)
    kb.row(bt3)
    bt0 = InlineKeyboardButton('Назад', callback_data='start')
    kb.row(bt0)
    return kb


async def kb_prof(a):
    kb = InlineKeyboardMarkup()
    for i in a:
        kb.row(InlineKeyboardButton(i[1], callback_data='prof_%s' % i[0]))
    bt0 = InlineKeyboardButton('Назад', callback_data='start')
    kb.row(bt0)
    return kb


async def kb_next():
    bt1 = InlineKeyboardButton('Другие вакансии', callback_data='next')
    bt2 = InlineKeyboardButton('В начало', callback_data='exit')
    kb = InlineKeyboardMarkup()
    kb.row(bt1)
    kb.row(bt2)
    return kb


async def kb_exit():
    bt0 = InlineKeyboardButton('В начало', callback_data='exit')
    kb = InlineKeyboardMarkup()
    kb.row(bt0)
    return kb


async def kb_admin():
    bt1 = InlineKeyboardButton('Вакансии', callback_data='admin_vac')
    bt2 = InlineKeyboardButton('Профессии', callback_data='admin_name')
    bt3 = InlineKeyboardButton('Институты', callback_data='admin_university')
    bt0 = InlineKeyboardButton('Выйти', callback_data='admin_exit')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2, bt3)
    kb.row(bt0)
    return kb


async def kb_edit(a):
    bt1 = InlineKeyboardButton('Добавить', callback_data='add')
    bt2 = InlineKeyboardButton('Удалить', callback_data='del')
    bt0 = InlineKeyboardButton('Назад', callback_data='admin_start')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2)
    kb.row(bt0)
    return kb


async def kb_admin_back(a):
    bt0 = InlineKeyboardButton('Назад', callback_data=a)
    kb = InlineKeyboardMarkup()
    kb.row(bt0)
    return kb


async def kb_admin_start():
    bt0 = InlineKeyboardButton('Назад', callback_data='admin_start')
    kb = InlineKeyboardMarkup()
    kb.row(bt0)
    return kb