from aiogram.utils import executor
from load_bot import bot
import database


async def on_shutdown(dp):
    await bot.close()


async def on_startup(dp):
    # await database.create_db()
    # await database.add_university('МФТИ')
    # await database.add_university('ВШЭ')
    # await database.add_university('МГУ')
    # await database.add_name('Веб-разработчик')
    # await database.add_name('Преподаватель')
    # await database.add_name('Администратор БД')
    # await database.add_vac('1', '1', 'https://mgutm.ru/2021/08/26/speczialist-tehnicheskoj-podderzhki-udalenno/')
    # await database.add_vac('2', '2', 'https://mgutm.ru/2021/07/22/coddy-prepodavatel-programmist/')
    # await database.add_vac('3', '3', 'https://mgutm.ru/2021/07/20/pervaya-rabota-konstruktor-baz-dannyh/')
    pass


if __name__ == "__main__":
    from menu import dp
    from admin import dp
    executor.start_polling(dp, on_shutdown=on_shutdown, on_startup=on_startup)
