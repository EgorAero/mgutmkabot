from load_bot import dp
from states import Steps
import database
import keyboards
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.memory import MemoryStorage


"""Функция, выполняющаяся при получении команды /start или /menu"""
@dp.message_handler(commands=['start', 'menu'])
async def send_welcome(message: types.Message):
    kb = await keyboards.kb_start()
    ans = "Привет! Этот бот поможет найти тебе вакансию.\nНажми кнопку \"Найти\":"
    await message.reply(ans, reply=False, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'start', state='*')
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Steps.inst.set()
    a = await database.show_university()
    kb = await keyboards.kb_inst(a)
    ans = "Выбери учебное заведение:"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(state=Steps.inst)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Steps.lvl.set()
    async with state.proxy() as data:
        data['university'] = call.data[11:]
    kb = await keyboards.kb_degr()
    ans = "Выбери уровень образования:"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'exit', state='*')
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await state.finish()
    kb = await keyboards.kb_start()
    ans = "Привет! Этот бот поможет найти тебе вакансию.\nНажми кнопку \"Найти\":"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(state=Steps.lvl)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Steps.num.set()
    if call.data == 'bac':
        kb = await keyboards.kb_bac()
    else:
        kb = await keyboards.kb_mag()
    ans = "Выбери курс"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(state=Steps.num)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Steps.what.set()
    a = await database.show_name()
    kb = await keyboards.kb_prof(a)
    ans = "Ваши предпочтения в профессии:"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'next', state=Steps.what)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        if data['db']:
            db = data['db'].pop()[3]
            ans = "%s" % db
            kb = await keyboards.kb_next()
        else:
            ans = "Вы просмотрели все вакансии"
            kb = await keyboards.kb_exit()
    await call.message.edit_text(ans, disable_web_page_preview=True, reply_markup=kb)


@dp.callback_query_handler(state=Steps.what)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    kb = await keyboards.kb_next()
    prof_name = call.data[5:]
    async with state.proxy() as data:
        data['db'] = await database.show_vacancy(prof_name, data['university'])
        if data['db']:
            db = data['db'].pop()[3]
            ans = "%s" % db
            kb = await keyboards.kb_next()
        else:
            ans = "Вакансий нет"
            kb = await keyboards.kb_exit()
    await call.message.edit_text(ans, disable_web_page_preview=True, reply_markup=kb)
