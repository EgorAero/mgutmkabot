from load_bot import dp, bot
from states import Admin, Vac, Name, Uni
import database
import keyboards
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.memory import MemoryStorage


@dp.message_handler(commands=['admin'])  
async def send_welcome(message: types.Message):
    await Admin.start.set()
    kb = await keyboards.kb_admin()
    ans = "Админ-панель\n--------------"
    await message.reply(ans, reply=False, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'admin_start', state='*')
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Admin.start.set()
    kb = await keyboards.kb_admin()
    ans = "Админ-панель\n--------------"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'admin_vac', state=Admin.start)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Admin.vac.set()
    kb = await keyboards.kb_edit(True)
    db = await database.show_vac()
    ans = "Редактирование ссылок на вакансии\n---"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[3])
    await call.message.edit_text(ans, reply_markup=kb, disable_web_page_preview=True)


@dp.callback_query_handler(lambda c: c.data == 'admin_name', state=Admin.start)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Admin.name.set()
    kb = await keyboards.kb_edit(False)
    db = await database.show_name()
    ans = "Редактирование названий вакансий\n---"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[1])
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'admin_university', state=Admin.start)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Admin.university.set()
    kb = await keyboards.kb_edit(False)
    db = await database.show_university()
    ans = "Редактирование названий институтов\n---"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[1])
    await call.message.edit_text(ans, reply_markup=kb)


# Редактор ссылок на вакансии
@dp.callback_query_handler(lambda c: c.data == 'add', state=Admin.vac)
async def vac_edit_1(call: types.CallbackQuery, state: FSMContext):
    await Vac.add.set()
    a = await database.show_university()
    kb = await keyboards.kb_inst(a)
    ans = "Выберите институт"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(state=Vac.add)
async def vac_edit_2(call: types.CallbackQuery, state: FSMContext):
    await Vac.add1.set()
    async with state.proxy() as data:
        data['university'] = call.data[11:]
    a = await database.show_name()
    kb = await keyboards.kb_name(a)
    ans = "Выберите вакансию"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(state=Vac.add1)
async def vac_edit_3(call: types.CallbackQuery, state: FSMContext):
    await Vac.add2.set()
    async with state.proxy() as data:
        data['name'] = call.data[5:]
    ans = "Отправьте ссылку на вакансию"
    kb = await keyboards.kb_admin_start()
    await call.message.edit_text(ans, reply_markup=kb)


@dp.message_handler(state=Vac.add2)
async def send_welcome(message: types.Message, state: FSMContext):
    await Admin.vac.set()
    kb = await keyboards.kb_edit(True)
    async with state.proxy() as data:
        db = await database.add_vac(data['name'], data['university'], message.text)
    ans = "Редактирование ссылок на вакансии\n---"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[3])
    await message.reply(ans, reply=False, reply_markup=kb, disable_web_page_preview=True)


@dp.callback_query_handler(lambda c: c.data == 'del', state=Admin.vac)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Vac.delete.set()
    kb = await keyboards.kb_admin_start()
    db = await database.show_vac()
    ans = "(Удаление вакансии)"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[3])
    ans += "\n(Введите ID вакансии)"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.message_handler(state=Vac.delete)
async def send_welcome(message: types.Message):
    db = await database.del_vac(message.text)
    await Admin.vac.set()
    kb = await keyboards.kb_edit(False)
    ans = "Редактирование вакансий\n---"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[3])
    await message.reply(ans, reply=False, reply_markup=kb, disable_web_page_preview=True)


# Редактор названий вакансий
@dp.callback_query_handler(lambda c: c.data == 'add', state=Admin.name)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Name.add.set()
    kb = await keyboards.kb_admin_start()
    ans = "Введите название вакансии"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.message_handler(state=Name.add)
async def send_welcome(message: types.Message):
    db = await database.add_name(message.text)
    await Admin.university.set()
    kb = await keyboards.kb_edit(False)
    ans = "Редактирование названий вакансий\n---"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[1])
    await message.reply(ans, reply=False, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'del', state=Admin.name)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Name.delete.set()
    kb = await keyboards.kb_admin_start()
    db = await database.show_name()
    ans = "(Удаление профессии)"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[1])
    ans += "\n(Введите ID профессии)"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.message_handler(state=Name.delete)
async def send_welcome(message: types.Message):
    db = await database.del_name(message.text)
    await Admin.university.set()
    kb = await keyboards.kb_edit(False)
    ans = "Редактирование названий вакансий\n---"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[1])
    await message.reply(ans, reply=False, reply_markup=kb)


# Редактор названий институтов
@dp.callback_query_handler(lambda c: c.data == 'add', state=Admin.university)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Uni.add.set()
    kb = await keyboards.kb_admin_start()
    ans = "Введите название института"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.message_handler(state=Uni.add)
async def send_welcome(message: types.Message):
    db = await database.add_university(message.text)
    await Admin.university.set()
    kb = await keyboards.kb_edit(False)
    ans = "Редактирование названий институтов\n---"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[1])
    await message.reply(ans, reply=False, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'del', state=Admin.university)
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await Uni.delete.set()
    kb = await keyboards.kb_admin_start()
    db = await database.show_university()
    ans = "(Удаление института из базы)"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[1])
    ans += "\n(Введите ID института)"
    await call.message.edit_text(ans, reply_markup=kb)


@dp.message_handler(state=Uni.delete)
async def send_welcome(message: types.Message):
    db = await database.del_university(message.text)
    await Admin.university.set()
    kb = await keyboards.kb_edit(False)
    ans = "Редактирование названий институтов\n---"
    for i in db:
        ans += '\n[%s] %s' % (i[0], i[1])
    await message.reply(ans, reply=False, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'admin_exit', state='*')
async def on_start_action(call: types.CallbackQuery, state: FSMContext):
    await state.finish()
    await bot.delete_message(chat_id=call.from_user.id, message_id=call.message.message_id)