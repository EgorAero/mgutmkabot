import sqlite3
from random import randint

async def create_db():
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS names(
                    id INTEGER PRIMARY KEY,
                    name TEXT);
                    """)
    cur.execute("""CREATE TABLE IF NOT EXISTS university(
                    id INTEGER PRIMARY KEY,
                    name TEXT);
                    """)
    cur.execute("""CREATE TABLE IF NOT EXISTS vacs(
                    id INTEGER PRIMARY KEY,
                    name_id INTEGER,
                    university_id INTEGER,
                    link INTEGER,
                    FOREIGN KEY (name_id) REFERENCES names(id)
                    FOREIGN KEY (university_id) REFERENCES university(id));
                    """)
    conn.commit()


# Названия вакансий
async def add_name(a):
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO names(name) VALUES('%s');
                """ % a)
    cur.execute("""SELECT * from names;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res


async def edit_name(a, b):
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""UPDATE names
                    SET 'name'='%s'
                    WHERE id = '%s';
                    """ % (a, b))
    cur.execute("""SELECT * from names
                    WHERE id = '%s';
                    """ % b)
    res = cur.fetchall()
    conn.commit()
    return res


async def show_name():
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""SELECT * from names;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res


async def del_name(a):
    conn = sqlite3.connect('vac.db')  # Подключаемся к БД
    cur = conn.cursor()
    cur.execute("""DELETE FROM names
                    WHERE id = '%s';
                    """ % a)
    cur.execute("""SELECT * from names;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res
#--------------------------#


# Названия институов
async def add_university(a):
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO university(name) VALUES('%s');
                """ % a)
    cur.execute("""SELECT * from university;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res
    

async def show_university():
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""SELECT * from university;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res


async def edit_university(a, b):
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""UPDATE university
                    SET 'name'='%s'
                    WHERE id = '%s';
                    """ % (a, b))
    cur.execute("""SELECT * from university
                    WHERE id = '%s';
                    """ % b)
    res = cur.fetchall()
    conn.commit()
    return res


async def del_university(a):
    conn = sqlite3.connect('vac.db')  # Подключаемся к БД
    cur = conn.cursor()
    cur.execute("""DELETE FROM university
                    WHERE id = '%s';
                    """ % a)
    cur.execute("""SELECT * from university;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res
#--------------------------#


# Вакансии
async def add_vac(a, b, c):
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO vacs(name_id, university_id, link) VALUES('%s', '%s', '%s');
                """ % (a, b, c))
    cur.execute("""SELECT * from vacs;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res


async def show_vac():
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""SELECT * from vacs;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res


async def edit_vac(a, b):
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""UPDATE vacs
                    SET 'name'='%s'
                    WHERE id = '%s';
                    """ % (a, b))
    cur.execute("""SELECT * from university
                    WHERE id = '%s';
                    """ % b)
    res = cur.fetchall()
    conn.commit()
    return res


async def del_vac(a):
    conn = sqlite3.connect('vac.db')  # Подключаемся к БД
    cur = conn.cursor()
    cur.execute("""DELETE FROM vacs
                    WHERE id = '%s';
                    """ % a)
    cur.execute("""SELECT * from vacs;
                    """)
    res = cur.fetchall()
    conn.commit()
    return res


async def show_vacancy(a, b):
    conn = sqlite3.connect('vac.db')
    cur = conn.cursor()
    cur.execute("""SELECT * from vacs
                    WHERE name_id = '%s'
                    AND university_id = '%s';
                    """ % (a, b))
    res = cur.fetchall()
    conn.commit()
    return res