from aiogram.dispatcher.filters.state import State, StatesGroup

class Steps(StatesGroup):
    inst = State()
    lvl = State()
    num = State()
    what = State()


class Admin(StatesGroup):
    start = State()
    vac = State()
    name = State()
    university = State()


class Vac(StatesGroup):
    add = State()
    add1 = State()
    add2 = State()
    edit = State()
    delete = State()


class Name(StatesGroup):
    add = State()
    edit = State()
    delete = State()


class Uni(StatesGroup):
    add = State()
    edit = State()
    delete = State()